
# export pd dataframe to emme format file
# sub is list of form [['col1', value], ['col2', value],...]
def exportEmme(input, outputFolder, source = None,
               columns = ("Origin", "Destination", "Trips"), 
               sub = [], 
               add = None, 
               emmeHeader = "t matrices\nd matrix= mf001\na matrix= mf001 temp1 0 ", 
               ext = ".txt", 
               space = True):
    
    import pandas 
    
    # Subset the input based on sub list argument
    # Names of the sub list should be column names in input
    if(len(sub) > 0):
        for item in sub:
            input = input[:, item[0] == item[1]]
    
    
    print("incomplete")
    


'''
# Once subset is defined, select the 3 columns needed
  input <- input[,columns]

  # Add a data frame with the same 3 columns if it doesn't already exist
  # And order it based on the first and second origin/destination columns
  if(!is.null(add)) {
    input <- do.call(rbind, list(input, add))
    if(!any(grepl("^[A-Za-z]+$", input[,1], perl = T))) input[,1] <- as.numeric(input[,1])
    if(!any(grepl("^[A-Za-z]+$", input[,2], perl = T))) input[,2] <- as.numeric(input[,2])
    input <- input[order(input[,1], input[,2]),]
  }


  # Create an emme format column
  if(space == TRUE) {
    if(nrow(input) > 0) input <- data.frame(All = paste0(" ", input[,1], " ", input[,2], ": ", input[,3]),
                                            stringsAsFactors = FALSE)
  } else {
    if(nrow(input) > 0) input <- data.frame(All = paste0(" ", input[,1], " ", input[,2], ":", input[,3]),
                                            stringsAsFactors = FALSE)
  }

  # Open file connection and write the header the final emme format column
  # Name of the file connection depends on the sub list argument defined
  output <- file(paste0(outputFolder, "\\",
                        source,
                        if(!is.null(source) & length(sub) > 0) "_",
                        if(length(sub) > 0) paste(as.vector(sub), collapse = "_"),
                        ext))
  if(nrow(input) > 0) writeLines(paste0(emmeHeader, "\n", paste(input$All, collapse = "\n")),
                                 output)
  if(nrow(input) == 0) writeLines(emmeHeader, output)
  close(output)

}
'''