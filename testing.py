# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 08:08:36 2018

@author: joanna.watts
"""

import os

os.chdir('C:\\Users\\Joanna.Watts\\Documents\\JW\\Python\\EmmeIO_py')

# testing import Emme
from emmeIO import importEmme

filename = 'data\\example_OD.txt'

data = importEmme(filename)

print(data)


# testing emmeToCsv

from emmeIO import emmeToCsv

inputFileName = 'data\\example_OD.txt'
outputFileName = 'tests\\temp_example_output.csv'

emmeToCsv(inputFileName, outputFileName)

os.remove(outputFileName)