# imports from emme file, converts to array
# skips number of lines specified by skiplines then skips lines starting with t, c, d or a
# requires pandas
def importEmme(filename, skiplines = 0):
    
    import pandas as pd
    
    file = open(filename, 'r')
    for i in range(0,skiplines):
        file.readline()
    input = file.readlines()
    file.close()
    
    output = pd.DataFrame(
            {"Origin" : [],
             "Destination" : [],
             "Trips" : []})
    
    for line in input:
        line = line.split(' ')
        
        # neaten line
        if(line[-1][-1] == '\n'): line[-1] = line[-1][0:-1]
        if(line[0] == ''): line = line[1:]
        if(line[1][-1] == ':'): line[1] = line[1][:-1]
        
        # skip lines starting with t, c, d
        if(line[0][0] in ('t', 'c', 'd', 'a')): continue
    
        if(len(line) == 3):
            temp = pd.DataFrame(
                    {"Origin" : [float(line[0])],
                     "Destination" : [float(line[1])],
                     "Trips" : [float(line[2])]})
            output=pd.concat([output, temp], axis=0)
    #end for line in input
    
    return output


# converts file from emme to csv format
def emmeToCsv(inputFileName, outputFileName, inputSkipLines = 0):
    # pandas used for to_csv, input is a pandas dataframe
    import pandas
    from emmeIO import importEmme
    
    input = importEmme(inputFileName, inputSkipLines)
    
    input.to_csv(outputFileName, index = False)