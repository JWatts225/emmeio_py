Functions for importing and exporting emme files.


Structure:

- Completed functions in emmeIO.py.

- In progress functions in relevant named files.

- Testing of functions/examples of how to use in testing.py.

- Data for examples in data/, examples output to tests/